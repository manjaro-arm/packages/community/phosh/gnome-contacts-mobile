# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Fabian Bornschein <fabiscafe-at-mailbox-dot-org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=gnome-contacts-mobile
pkgver=45.1
pkgrel=1
pkgdesc="Contacts Manager for GNOME with Purism patches"
arch=(x86_64 armv7h aarch64)
url="https://wiki.gnome.org/Apps/Contacts"
license=(GPL2)
depends=(
  cairo
  dconf
  evolution-data-server
  folks
  gdk-pixbuf2
  glib2
  gnome-online-accounts
  gtk4
  hicolor-icon-theme
  libadwaita
  libgee
  libgoa
  libportal
  libportal-gtk4
  pango
  qrencode
)
makedepends=(
  appstream-glib
  git
  gobject-introspection
  meson
  vala
)
groups=(gnome)
provides=("gnome-contacts=$pkgver")
conflicts=(gnome-contacts)
_commit=aa0456c32a6ec8e766ea090e7f3fb85e0035f506  # tags/45.1^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-contacts.git#commit=$_commit"
        0001-ContactSheet-Add-make-call-and-send-sms-button.patch
#        0002-Add-DBus-API-for-adding-contact-with-preset-properties.patch
)
b2sums=('SKIP'
        'fa242d8e587486496502cafbb565423752c7f3d49c15bc364a26f64bb81ddbe070f900fa68c8c24d863a894123416ecef7b9b9902d2acbe077523a036d3ccf5c')

pkgver() {
  cd gnome-contacts
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd gnome-contacts

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  arch-meson gnome-contacts build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
}
